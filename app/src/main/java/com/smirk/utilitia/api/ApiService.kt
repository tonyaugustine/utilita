package com.smirk.utilitia.api

import androidx.lifecycle.LiveData
import com.smirk.utilitia.model.content.StatusModel
import retrofit2.http.GET

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */
interface ApiService {

    @GET(STATUSES)
    fun getStatuses() : LiveData<ApiResponse<StatusModel>>
}