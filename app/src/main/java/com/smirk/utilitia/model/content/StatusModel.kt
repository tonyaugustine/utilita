package com.smirk.utilitia.model.content

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Tony Augustine on 22,January,2021
 * tonyaugustine47@gmail.com
 */
data class StatusModel(
    @field:SerializedName("APIs & DBs")
    @Expose
    val aPIsDBs: Map<String, Status>,
    @field:SerializedName("Sites")
    @Expose
    val sites: Map<String, Status>
) {
    var apiDbsList =  ArrayList<Status>()
    var sitesList = ArrayList<Status>()
}