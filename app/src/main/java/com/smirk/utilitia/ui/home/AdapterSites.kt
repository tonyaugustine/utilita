package com.smirk.utilitia.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.smirk.utilitia.AppExecutors
import com.smirk.utilitia.R
import com.smirk.utilitia.base.BaseDataBindListAdapterWithPosition
import com.smirk.utilitia.base.DataBoundListAdapterType
import com.smirk.utilitia.databinding.InflateApisDbsBinding
import com.smirk.utilitia.model.content.Status

class AdapterSites(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    val callback: (Status) -> Unit
) :
    BaseDataBindListAdapterWithPosition<Status, InflateApisDbsBinding>(appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<Status>() {
            override fun areItemsTheSame(oldItem: Status, newItem: Status): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areContentsTheSame(oldItem: Status, newItem: Status): Boolean {
                return oldItem.url.equals(newItem.url)
            }

        }) {

    override fun createBinding(parent: ViewGroup, viewType: Int): InflateApisDbsBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.inflate_apis_dbs, parent,
            false, dataBindingComponent
        )

    }

    override fun bind(binding: InflateApisDbsBinding, item: Status, position: Int) {

        binding.data = item

        if (position == itemCount - 1) {
            binding.seperator.visibility = View.GONE
        } else {
            binding.seperator.visibility = View.VISIBLE
        }

        binding.root.setOnClickListener {
            binding.data?.let {
                callback(it)
            }
        }

    }


}