package com.smirk.utilitia.di

import com.smirk.utilitia.ui.home.HomeFragment
import com.smirk.utilitia.ui.detail.DetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Tony Augustine on 29,September,2019
 * tonyaugustine47@gmail.com
 */
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment() : HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailFragment() : DetailFragment
}