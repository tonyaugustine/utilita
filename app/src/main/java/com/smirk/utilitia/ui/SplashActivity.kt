package com.smirk.utilitia.ui

import android.content.Intent
import android.os.Bundle
import com.smirk.utilitia.base.BaseActivity
import com.smirk.utilitia.ui.home.HomeActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}
