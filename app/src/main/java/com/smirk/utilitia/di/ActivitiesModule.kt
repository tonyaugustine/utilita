package com.smirk.utilitia.di

import com.smirk.utilitia.ui.SplashActivity
import com.smirk.utilitia.ui.home.HomeActivity
import com.smirk.utilitia.ui.detail.DetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */
@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeHomeActivity() : HomeActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributePlayerActivity() : DetailActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeSplashActivity() : SplashActivity

}