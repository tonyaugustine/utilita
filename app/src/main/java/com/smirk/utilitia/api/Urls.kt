package com.smirk.utilitia.api

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */

const val BASE_URL: String = "http://private-176645-utilita.apiary-mock.com/"

const val STATUSES = "status"