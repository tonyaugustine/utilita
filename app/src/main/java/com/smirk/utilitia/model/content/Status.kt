package com.smirk.utilitia.model.content

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Tony Augustine on 22,January,2021
 * tonyaugustine47@gmail.com
 */
data class Status(
    val responseTime: Double? = null,
    @field:SerializedName("class")
    @Expose
    val jsonMemberClass: String? = null,
    val url: String? = null,
    val responseCode: Int? = null
): Serializable {
    var title: String? = null
}
