package com.smirk.utilitia.ui.detail


import android.os.Bundle
import androidx.fragment.app.Fragment

import com.smirk.utilitia.R
import com.smirk.utilitia.base.BaseFragment
import com.smirk.utilitia.databinding.FragmentDetailBinding
import com.smirk.utilitia.model.content.Status

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlayersFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailFragment : BaseFragment<FragmentDetailBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_detail
    }


    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }


    companion object {

        @JvmStatic
        fun newInstance(status : Status) =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM1, status)
                }
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initUi()

    }

    private fun initUi() {

        val data :Status = arguments?.getSerializable(ARG_PARAM1) as Status

        mBinding.status = data

        /*name.text = data.title
        if (!data.url.isNullOrEmpty()) {
            url.text = data.url
        }
        time.text = data.responseTime.toString()
        code.text = data.responseCode.toString()*/

    }
}
