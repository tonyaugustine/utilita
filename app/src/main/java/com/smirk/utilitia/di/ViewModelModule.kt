package com.smirk.utilitia.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.smirk.utilitia.viewmodel.ViewModelFactory
import com.smirk.utilitia.viewmodel.ViewModelHome
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ViewModelHome::class)
    abstract fun bindHomeViewModel(homViewModel : ViewModelHome) : ViewModel

}