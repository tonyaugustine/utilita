package com.smirk.utilitia.ui.home

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smirk.utilitia.R
import com.smirk.utilitia.base.BaseActivity
import com.smirk.utilitia.databinding.ActivityHomeBinding
import com.smirk.utilitia.viewmodel.ViewModelFactory
import com.smirk.utilitia.viewmodel.ViewModelHome
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

/**
 * Created by Tony Augustine on 21,January,2021
 * tonyaugustine47@gmail.com
 */
class HomeActivity : BaseActivity() {

    lateinit var viewModelHome: ViewModelHome

    @Inject
    lateinit var mViewModelFactory: ViewModelFactory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        viewModelHome =
            ViewModelProviders.of(this, mViewModelFactory).get(ViewModelHome::class.java)

        supportFragmentManager.beginTransaction()
            .replace(R.id.continerLayout, HomeFragment.newInstance("", "")).commit()

        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = "Utilita"


        viewModelHome.getStatuses.observe(this, Observer {response ->

            if (response.data == null)
                return@Observer

            progressBar.visibility = View.GONE
            viewModelHome.setHomeData(response.data)

        })
    }
}