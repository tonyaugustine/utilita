package com.smirk.utilitia.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.smirk.utilitia.model.content.Status
import com.smirk.utilitia.model.content.StatusModel
import com.smirk.utilitia.repository.IncrowdRepo
import javax.inject.Inject

/**
 * Created by Tony Augustine on 29,September,2019
 * tonyaugustine47@gmail.com
 */
class ViewModelHome
@Inject constructor(repo : IncrowdRepo) : ViewModel() {

    private var homeData = MutableLiveData<StatusModel>()

    val getStatuses = repo.getStatuses()

    fun setHomeData(homeData : StatusModel) {
        this.homeData.value = homeData
    }

    fun getHomeData() : LiveData<StatusModel> {
        return homeData
    }

}