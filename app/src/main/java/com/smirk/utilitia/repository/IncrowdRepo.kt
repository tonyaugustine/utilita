package com.smirk.utilitia.repository

import androidx.lifecycle.LiveData
import com.smirk.utilitia.AppExecutors
import com.smirk.utilitia.api.ApiResponse
import com.smirk.utilitia.api.ApiService
import com.smirk.utilitia.api.ApiSuccessResponse
import com.smirk.utilitia.model.content.Status
import com.smirk.utilitia.model.content.StatusModel
import com.smirk.utilitia.vo.Resource
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Tony Augustine on 21,January,2021
 * tonyaugustine47@gmail.com
 */
@Singleton
class IncrowdRepo @Inject constructor(
    private val appExecutors: AppExecutors,
    private val apiService: ApiService
) {

    fun getStatuses(): LiveData<Resource<StatusModel>> {
        return object : NetworkBoundResourceNoCache<StatusModel>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<StatusModel>> {
                return apiService.getStatuses()
            }


            override fun processResponse(response: ApiSuccessResponse<StatusModel>): StatusModel {
                val statusModel = response.body

                if (!statusModel.aPIsDBs.isNullOrEmpty()) {
                    val listApisDbs = ArrayList<Status>()
                    statusModel.aPIsDBs.forEach {
                        val status = it.value
                        status.title = it.key
                        listApisDbs.add(status)
                    }
                    statusModel.apiDbsList = listApisDbs
                }

                if (!statusModel.sites.isNullOrEmpty()) {
                    val listSites = ArrayList<Status>()
                    statusModel.sites.forEach {
                        val site = it.value
                        site.title = it.key
                        listSites.add(site)
                    }

                    statusModel.sitesList = listSites
                }

                return statusModel
            }

        }.asLiveData()
    }

}