package com.smirk.utilitia.ui.home


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.smirk.utilitia.AppExecutors

import com.smirk.utilitia.R
import com.smirk.utilitia.base.BaseFragment
import com.smirk.utilitia.databinding.FragmentHomeBinding
import com.smirk.utilitia.ui.detail.DetailActivity
import com.smirk.utilitia.utils.autoCleared
import com.smirk.utilitia.viewmodel.ViewModelHome
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private var adapterSites by autoCleared<AdapterSites>()
    private var adapterApisDbs by autoCleared<AdapterApisDbs>()

    @Inject
    lateinit var appExecutors : AppExecutors
    private lateinit var viewModel : ViewModelHome

    override fun getLayoutId(): Int {
        return R.layout.fragment_home
    }

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

         activity?.let {
             viewModel = getViewModelShared(it, ViewModelHome::class.java)
        }

        initUi()
        observe()
    }

    private fun initUi() {

        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerViewApisDbs.layoutManager = llm

//        val itemDecoration = DividerItemDecoration(context, llm.orientation)
//        recyclerViewOfficials.addItemDecoration(itemDecoration)

        adapterSites = AdapterSites(dataBindingComponent, appExecutors) {

            val intent = Intent(activity, DetailActivity::class.java).apply {
                putExtra("data", it)
            }
            startActivity(intent)
        }

        recyclerViewStatus.adapter = adapterSites

        adapterApisDbs = AdapterApisDbs(dataBindingComponent, appExecutors) {
            val intent = Intent(activity, DetailActivity::class.java).apply {
                putExtra("data", it)
            }
            startActivity(intent)
        }

        recyclerViewApisDbs.adapter = adapterApisDbs

    }

    private fun observe() {
        viewModel.getHomeData().observe(this, Observer {response ->

            if (response == null)
                return@Observer


            adapterApisDbs.submitList(response.apiDbsList)

            adapterSites.submitList(response.sitesList)

        })
    }
}
