package com.smirk.utilitia.ui.detail

import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.smirk.utilitia.R
import com.smirk.utilitia.base.BaseActivity
import com.smirk.utilitia.databinding.ActivityDetailBinding
import com.smirk.utilitia.model.content.Status
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : BaseActivity() {

    lateinit var dataBinding : ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent.hasExtra("data")) {

            val data = intent.getSerializableExtra("data") as Status
            if (!data.title.isNullOrEmpty()) {
                supportActionBar?.title = data.title
            }
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, DetailFragment.newInstance(data)).commit()
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }

        }
        return true
    }
}
